import React from 'react';

const hints = [
    {id: 1, hint: "Hint 1"},
    {id: 2, hint: "Hint 2"},
    {id: 3, hint: "Hint 3"}
]

const AddQuestion = () => {
    return (
        <div>
            <div className="row">
                <div className="col-lg-12">
                    <div className="card shadow" >
                        <span  className="card-header bg-dark text-white rounded-top">QUESTION FORM</span>
                        <div className="card-body card-block">
                            <form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                                <div className="row form-group">
                                    <div className="col col-md-3">
                                        <label htmlFor="textarea-input" className=" form-control-label">Question</label>
                                    </div>
                                    <div className="col-12 col-md-9">
                                        <textarea name="textarea-input" id="textarea-input" rows={2}  className="form-control"></textarea>
                                        <small className="form-text text-muted">   Enter the question</small>
                                    </div>
                                </div> 
                                <div className="row form-group">
                                    <div className="col col-md-3">
                                        <label htmlFor="file-input" className=" form-control-label">Image Input</label>
                                    </div>
                                    <div className="col-12 col-md-9">
                                        <input type="file" id="file-input" name="file-input" className="form-control-file" />
                                    </div>
                                </div><br/>
                                {hints.map((key) => {
                                    return (
                                        <div key={key.id} className="row form-group">
                                            <div className="col col-md-3">
                                                <label htmlFor="textarea-input" className=" form-control-label">{key.hint}</label>
                                            </div>
                                            <div className="col-12 col-md-9">
                                                <textarea name="textarea-input" id="textarea-input" rows={3} placeholder="Hint here" className="form-control"></textarea>
                                            </div>
                                        </div>
                                    )
                                })}
                            </form>
                        </div>
                        <div className="card-footer">
                            <button type="submit" className="btn btn-primary float-right">
                                <i className="fa fa-dot-circle-o"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AddQuestion;