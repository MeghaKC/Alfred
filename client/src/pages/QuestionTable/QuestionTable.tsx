import React, {useState, useEffect} from 'react';

type TableProps = {
    qn: string,
    index: number
}

const meta = ['QUESTION NUMBER', 'QUESTION', 'QUESTION DELETE']

const questions = [
    {id: 1, qn: 'how many bits make a byte'},
    {id: 2, qn: 'inventor of difference engine'},
]

const Row = ({qn, index}: TableProps) => {
    return (
        <tr className="tr-shadow" key={index}>
            <td>{index}.</td>
            <td>{qn}</td>
            <td>
                <div className="table-data-feature">
                    <button className="item" data-toggle="tooltip" data-placement="top" title="Delete">
                        <i className="zmdi zmdi-delete"></i>
                    </button>
                </div>
            </td>
        </tr>
    )
} 

const QuestionTable = () => {
    const [tableData, setTableData] = useState<{ id: number; qn: string; }[]>([])

    useEffect(() => {
        setTableData(questions);
    }, []);

    return (
    	<div>
            <div className="row">
                <div className="col-lg-12">
                    <div className="row">
                        <div className="col-md-12">  
                            <div className="table-data__tool.right" style={{textAlign:'right'}}>
                                <div className="table-data__tool-right">
                                    <button className="au-btn au-btn-icon au-btn--green au-btn--small">
                                    <i className="zmdi zmdi-plus"></i>add item</button>                          
                                </div>
                            </div>
                        </div>
                    </div>
					<br/>
                    <div className="col-lg-12">
                        <div className="table-responsive table--no-card m-b-30">
                            <table className="table table-borderless table-striped table-earning">
                                <thead>
                                    <tr>{meta.map((key, index) => {
                                            return <th key={key}>{key}</th>
                                            }
                                        )}
                                    </tr>
                                </thead>
                                    <tbody>
                                        {tableData.map((key, index)=> (<Row qn={key.qn} index={key.id}/>))}
                                    </tbody>
							</table>
                         </div>
                    </div>                           
                </div>
			</div>
		</div>
    );
}

export default QuestionTable;